# SPDX-FileCopyrightText: Fraunhofer IML
#
# SPDX-License-Identifier: GPL-3.0-or-later

# context.area: VIEW_3D

"""A script to generate bounding box annotations from a Blender scene.

The script uses the Blender API to render a scene from multiple camera angles
and saves the rendered images to disk. The script also renders the segmentation.

Additionally, the script computes the bounding boxes of all trucks in the scene
as seen from each camera and saves the bounding boxes in the MOT format.
Refer to output2mot.py for a script to generate a compliant MOT dataset from the output.

Unfortunatly, this file cannot be split into multiple files because Blender
does not support importing modules from other files.
"""
import csv
import os
import random
import shutil
from functools import lru_cache
from pathlib import Path

import bpy
import cv2
import numpy as np
from mathutils import Matrix, Vector

SCENE_PATH = Path("./blender-scenes/complex-logistics-yard-truck-scene/")
OUTPUT_LOCATION = SCENE_PATH / Path("output/")
COMPOSITOR_FILE_OUTPUT_BASE_PATH = SCENE_PATH / Path("output/compositing_output")


def render_scene(scene: bpy.types.Scene, cam_ob: bpy.types.Camera) -> None:
    """Render a scene as seen from a camera."""
    # Set the camera as the active camera
    scene.camera = cam_ob

    # Render the scene
    bpy.ops.render.render(write_still=True)


def clamp(x: float, minimum: float, maximum: float) -> float:
    """Clamp (limit) a value between a minimum and maximum value."""
    return max(minimum, min(x, maximum))


def render_bounding_box_2d(
    scene: bpy.types.Scene, cam_obj: bpy.types.Object, mesh_obj: bpy.types.Object
) -> tuple[int, int, int, int]:
    """Returns camera space bounding box of the mesh object.

    Args:
        scene (bpy.types.Scene): The scene to use.
        cam_obj (bpy.types.Camera): The camera to use for the perspective.
        mesh_obj (bpy.types.Mesh): The mesh object to compute the bounding box for.

    Returns:
        tuple[int, int, int, int]: A tuple of (x, y, width, height) of the bounding box.
    """
    cam_to_world: Matrix = cam_obj.matrix_world
    world_to_cam = cam_to_world.normalized().inverted()
    dependency_graph = bpy.context.evaluated_depsgraph_get()
    mesh_eval: bpy.types.Object = mesh_obj.evaluated_get(dependency_graph)
    mesh_to_world: Matrix = mesh_obj.matrix_world

    # transform mesh to camera space
    mesh = mesh_eval.to_mesh()
    mesh.transform(mesh_to_world)
    mesh.transform(world_to_cam)

    camera: bpy.types.Camera = cam_obj.data
    frame: list[Vector] = [-v for v in camera.view_frame(scene=scene)[:3]]
    camera_is_persp = camera.type != "ORTHO"

    x_coords = []
    y_coords = []

    for v in mesh.vertices:
        co_local = v.co
        z = -co_local.z

        if camera_is_persp:
            if z == 0.0:
                x_coords.append(0.5)
                y_coords.append(0.5)
            else:
                frame = [(v / (v.z / z)) for v in frame]

        min_x, max_x = frame[1].x, frame[2].x
        min_y, max_y = frame[0].y, frame[1].y

        x = (co_local.x - min_x) / (max_x - min_x)
        y = (co_local.y - min_y) / (max_y - min_y)

        x_coords.append(x)
        y_coords.append(y)

    min_x = clamp(min(x_coords), 0.0, 1.0)
    max_x = clamp(max(x_coords), 0.0, 1.0)
    min_y = clamp(min(y_coords), 0.0, 1.0)
    max_y = clamp(max(y_coords), 0.0, 1.0)

    mesh_eval.to_mesh_clear()

    render = scene.render
    resolution_factor = render.resolution_percentage * 0.01
    dim_x = render.resolution_x * resolution_factor
    dim_y = render.resolution_y * resolution_factor

    # Check if the bounding box is valid
    if round((max_x - min_x) * dim_x) == 0 or round((max_y - min_y) * dim_y) == 0:
        return (0, 0, 0, 0)

    return (
        round(min_x * dim_x),  # X
        round(dim_y - max_y * dim_y),  # Y
        round((max_x - min_x) * dim_x),  # Width
        round((max_y - min_y) * dim_y),  # Height
    )


def recursive_bound_box_vertices_world(obj: bpy.types.Object) -> list[Vector]:
    """Get the bound box vertices of all meshes of an object in world space.

    This method also works for objects with nested meshes because it is recursive
    and traverses the whole object tree.

    Args:
        obj (bpy.types.Object): The parent Blender object

    Returns:
        List[Vector]: A flattened list of 8 vertices per mesh world space coordinates
                      describing the bound box per mesh.
    """
    if obj.type == "MESH":
        return [obj.matrix_world @ Vector(v) for v in obj.bound_box]
    else:
        vertices: list[Vector] = []
        for c in obj.children:
            vertices += recursive_bound_box_vertices_world(c)
        return vertices


def create_cube(
    translation: tuple[float, float, float],
    scale_factors: tuple[float, float, float],
    name: str = "BoundingBox",
) -> bpy.types.Object:
    """Utility function to create a cube object.

    Args:
        translation (Tuple[float, float,float]): translation of the cube from world origin
        scale_factors (Tuple[float, float,float]): scaling factors along the x, y and z axis
        name (str, optional): Blender object name. Defaults to "BoundingBox".

    Returns:
        bpy.types.Object: The Blender object of the newly created cube
    """
    # Definition of Cube vertices and faces
    verts = [
        (1.0, 1.0, -1.0),
        (1.0, -1.0, -1.0),
        (-1.0, -1.0, -1.0),
        (-1.0, 1.0, -1.0),
        (1.0, 1.0, 1.0),
        (1.0, -1.0, 1.0),
        (-1.0, -1.0, 1.0),
        (-1.0, 1.0, 1.0),
    ]

    faces = [(0, 1, 2, 3), (4, 7, 6, 5), (0, 4, 5, 1), (1, 5, 6, 2), (2, 6, 7, 3), (4, 0, 3, 7)]

    # Construction of object and mesh
    mesh = bpy.data.meshes.new(name)
    mesh.from_pydata(verts, [], faces)

    obj = bpy.data.objects.new(name, mesh)
    bpy.context.scene.collection.objects.link(obj)

    bpy.context.view_layer.objects.active = obj
    obj.select_set(True)

    # Translate and scale the cube to fit
    translation_matrix = Matrix.Translation(translation)
    scale_matrix = Matrix.Scale(scale_factors[0] / 2.0, 4, (1, 0, 0))
    scale_matrix @= Matrix.Scale(scale_factors[1] / 2.0, 4, (0, 1, 0))
    scale_matrix @= Matrix.Scale(scale_factors[2] / 2.0, 4, (0, 0, 1))

    obj.matrix_world @= translation_matrix @ scale_matrix

    return obj


@lru_cache
def name_to_color(name: str) -> tuple[int, int, int]:
    """Convert a string to a random deterministic color.

    Uses a new random object instance to avoid messing with the global random state.

    Args:
        name (str): The string to convert

    Returns:
        Tuple[int, int, int]: A tuple of three integers representing the RGB color values (0-255)
    """
    rnd = random.Random(name)
    return (rnd.randint(0, 255), rnd.randint(0, 255), rnd.randint(0, 255))


def draw_bounding_boxes(
    filepath: str,
    bounding_boxes: dict[str, tuple[int, int, int, int]],
    colors: dict[str, tuple[int, int, int]],
    suffix: str = "_bb",
) -> None:
    """Draw all bounding boxes onto a copy of the image.

    Args:
        filepath (str): The path to the original blank image
        bounding_boxes (Dict[str, Tuple[int, int, int, int]]): Mapping bounding box names
        to bouding box coordinates
        colors (Dict[str, Tuple[int, int, int]]): Mapping bounding box names to colors
        suffix (str, optional): The suffix to add to the new image file. Defaults to "_bb".
    """
    image = cv2.imread(filepath)
    for name, bounding_box in bounding_boxes.items():
        cv2.rectangle(
            image,
            (bounding_box[0], bounding_box[1]),
            (bounding_box[0] + bounding_box[2], bounding_box[1] + bounding_box[3]),
            colors[name],
            2,
        )

    # add the suffix to the filename
    _, file_extension = os.path.splitext(filepath)
    cv2.imwrite(filepath.replace(file_extension, f"{suffix}{file_extension}"), image)


def compute_occluded_bounding_boxes_2d(
    camera_name: str, truck_names: list[str]
) -> dict[str, tuple[int, int, int, int]]:
    """Compute the (occluded) 2D bounding boxes of trucks in the scene as seen by the camera.

    These bounding boxes show the whole object, even if it is occluded by other objects.

    Args:
        camera_name (str): the name of the camera object to use
        truck_names (List[str]): the names of the truck objects to compute the bounding boxes for

    Returns:
        Dict[str, Tuple[int, int, int, int]]: A dictionary mapping truck names to bounding boxes
    """
    bounding_boxes: dict[str, tuple[int, int, int, int]] = {}
    for truck_name in truck_names:
        truck_obj = bpy.data.objects[truck_name]
        vertices = recursive_bound_box_vertices_world(truck_obj)

        # Find the minimum and maximum values along each axis
        min_values = np.min(vertices, axis=0)
        max_values = np.max(vertices, axis=0)

        center = (min_values + max_values) / 2
        size = max_values - min_values

        bounding_box_obj = create_cube(center, size)

        bounding_box = render_bounding_box_2d(
            bpy.context.scene, bpy.data.objects[camera_name], bounding_box_obj
        )
        print(f"Located Bounding Box for '{truck_name}' at: {bounding_box}")

        # check if bouding box contains pixels
        if bounding_box[2] > 0 and bounding_box[3] > 0:
            bounding_boxes[truck_name] = bounding_box

        # delete the temporary bounding box cube after computing the 2D bounding box
        bpy.data.objects.remove(bounding_box_obj, do_unlink=True)
    return bounding_boxes


def extract_bouding_box_from_segmentation(file_path: str) -> tuple[int, int, int, int] | None:
    """Extract the 2D bounding box from a grayscale segmentation image.

    The image should contain only one segmentation mask for the object of interest.
    The image should be 8-bit grayscale with the background being 0.

    Args:
        file_path (str): the path to the segmentation image

    Returns:
        Tuple[int, int, int, int]: 2D Boundin Box (x, y, w, h) or None
    """
    image = cv2.imread(file_path, cv2.IMREAD_GRAYSCALE)

    if image is None:
        raise RuntimeError(f"Could not read image at '{file_path}'")

    np_image = np.array(image)
    segmentation_values = [v for v in np.unique(np_image) if v != 0]

    if len(segmentation_values) == 0:
        # the object is not visible in the image
        return None
    elif len(segmentation_values) != 1:
        raise ValueError(
            f"Expected exactly one segmentation value but found {len(segmentation_values)} "
            + f"in '{file_path}'"
        )

    segmentation_value = segmentation_values[0]
    segmentation_mask = np.where(np_image == segmentation_value)

    if len(segmentation_mask) and len(segmentation_mask[0]) and len(segmentation_mask[1]):
        y_min = np.min(segmentation_mask[0])
        y_max = np.max(segmentation_mask[0])
        x_min = np.min(segmentation_mask[1])
        x_max = np.max(segmentation_mask[1])

        (width, height) = (x_max - x_min, y_max - y_min)

        if width == 0 or height == 0:
            # the boudning box contains no pixels
            return None

        return (x_min, y_min, width, height)
    else:
        raise ValueError(
            f"No bounding box found for segmentation value {segmentation_value} in '{file_path}'"
        )


def copy_compositor_output(
    output_location: Path, frame_number: int, camera_name: str, copy_segmentations: bool = False
) -> None:
    """Copy the images from the composition file output.

    Copy the images from the composition file output location,
    e.g. output/compositing_output/ImageRGB_0040.jpg to the output_location,
    while renaming the files to include the camera name as a prefix,
    e.g. output_location/camera_1_ImageRGB_0040.jpg

    Args:
        output_location (str): The output location to copy the images to (directory))
        frame_number (int): The frame number to copy the images for
        camera_name (str): The name of the camera to include in the file name
        copy_segmentations (bool, optional): Whether to copy segmentation images. Defaults to False
    """
    if not (output_location.exists() and output_location.is_dir()):
        raise ValueError(
            f"Output location '{output_location}' does not exist or is not a directory"
        )

    # copy the rgb image, e.g. output/compositing_output/ImageRGB_0040.jpg
    shutil.copy2(
        COMPOSITOR_FILE_OUTPUT_BASE_PATH / f"ImageRGB_{frame_number:04d}.jpg",
        output_location / f"{camera_name}_ImageRGB_{frame_number:04d}.jpg",
    )

    if copy_segmentations:
        # glob all segmentation images, e.g. output/compositing_output/Segmentation2_0040.png
        for file_path in COMPOSITOR_FILE_OUTPUT_BASE_PATH.glob(
            f"Segmentation*_{frame_number:04d}.png"
        ):
            shutil.copy2(file_path, output_location / f"{camera_name}_{file_path.name}")


def save_bb_mot_format(
    mot_file_path: str,
    frame_number: int,
    track_ids: dict[str, int],
    bounding_boxes: dict[str, tuple[int, int, int, int]],
) -> None:
    """Save the bounding boxes in the MOT format.

    https://github.com/JonathonLuiten/TrackEval/blob/master/docs/MOTChallenge-Official/Readme.md#data-format

    Args:
        mot_file_path (str): The file to append the bounding boxes to.
        The file will be created if it does not exist.
        frame_number (int): The unique frame number
        track_ids (Dict[str, int]): The unique track id for each truck
        bounding_boxes (Dict[str, Tuple[int, int, int, int]]): The bounding boxes for each truck
    """
    # create an empty file if it does not exist
    if not os.path.exists(mot_file_path):
        with open(mot_file_path, "w", newline="\n") as mot_file:
            pass

    # use a csv writer to append the bounding boxes to the file
    with open(mot_file_path, "a", newline="\n") as mot_file:
        csv_writer = csv.writer(mot_file, delimiter=",")
        for truck_name, bounding_box in bounding_boxes.items():
            csv_writer.writerow(
                [
                    frame_number,  # frame id
                    track_ids[truck_name],  # track id
                    bounding_box[0],  # x
                    bounding_box[1],  # y
                    bounding_box[2],  # width
                    bounding_box[3],  # height
                    1,  # class id
                    1.0,  # score
                ]
            )


def render_current_frame(
    camera_names: list[str], truck_names: list[str], save_bb_images: bool = True
) -> dict[str, dict[str, tuple[int, int, int, int]]]:
    """Render the current frame from all cameras and compute the 2D bounding boxes for all trucks.

    Args:
        camera_names (List[str]): the names of the camera objects to use
        truck_names (List[str]): the names of the truck objects to compute the bounding boxes for
        save_bb_images (bool, optional): Whether to save copies of the images with
        bounding boxes in them. Defaults to True

    Returns:
        Dict[str, Dict[str, Tuple[int, int, int, int]]]: A dictionary mapping camera names to
        truck names to bounding boxes
    """
    # define a random color palette for the trucks
    truck_colors = {truck_name: name_to_color(truck_name) for truck_name in truck_names}

    # get the pass index for each truck
    truck_pass_ids = {
        truck_name: bpy.data.objects[truck_name].pass_index for truck_name in truck_names
    }
    if len(set(truck_pass_ids.values())) != len(truck_names):
        raise ValueError("Truck Pass IDs are not unique")

    occlouded_bounding_boxes: dict[str, dict[str, tuple[int, int, int, int]]] = {}
    visible_bounding_boxes: dict[str, dict[str, tuple[int, int, int, int]]] = {}

    for camera_name in camera_names:
        frame_number = bpy.context.scene.frame_current
        print("Processing camera", camera_name, "for frame", frame_number)

        visible_bounding_boxes[camera_name] = {}

        # Render the full scene with all outputs from the compositor
        render_scene(bpy.context.scene, bpy.data.objects[camera_name])
        copy_compositor_output(OUTPUT_LOCATION, frame_number, camera_name, copy_segmentations=True)

        # compute the bounding box for each truck in in the image
        occlouded_bounding_boxes[camera_name] = compute_occluded_bounding_boxes_2d(
            camera_name, truck_names
        )

        # iterate all segmentation images and extract the bounding boxes
        for truck_name, pass_id in truck_pass_ids.items():
            segmentation_file_path = (
                COMPOSITOR_FILE_OUTPUT_BASE_PATH / f"Segmentation{pass_id}_{frame_number:04d}.png"
            )
            bounding_box = extract_bouding_box_from_segmentation(
                str(segmentation_file_path.absolute())
            )
            if bounding_box is not None:
                visible_bounding_boxes[camera_name][truck_name] = bounding_box

        # save the bounding boxes in the MOT format
        mot_file_path = str(OUTPUT_LOCATION / f"{camera_name}_MOT.txt")
        save_bb_mot_format(
            mot_file_path, frame_number, truck_pass_ids, visible_bounding_boxes[camera_name]
        )

        if save_bb_images:
            rgb_image_path = str(
                OUTPUT_LOCATION / f"{camera_name}_ImageRGB_{frame_number:04d}.jpg"
            )
            draw_bounding_boxes(
                rgb_image_path,
                occlouded_bounding_boxes[camera_name],
                truck_colors,
                suffix="_occluded_bb",
            )
            draw_bounding_boxes(
                rgb_image_path,
                visible_bounding_boxes[camera_name],
                truck_colors,
                suffix="_visible_bb",
            )
    return occlouded_bounding_boxes


def render_all_frames(
    camera_names: list[str],
    truck_names: list[str],
    frame_start: int = bpy.context.scene.frame_start,
    frame_end: int = bpy.context.scene.frame_end,
) -> None:
    """Render all frames from frame_start to frame_end.

    Args:
        camera_names (List[str]): Name of cameras to render each frame from
        truck_names (List[str]): Name of trucks to compute bounding boxes for
        frame_start (int, optional): The first frame to render.
        Defaults to bpy.context.scene.frame_start.
        frame_end (int, optional): The last frame to render (inclusive).
        Defaults to bpy.context.scene.frame_end.
    """
    for frame in range(frame_start, frame_end + 1):
        print(f"Rendering frame {frame} of {frame_end}")
        bpy.context.scene.frame_set(frame)
        render_current_frame(camera_names, truck_names)


# Blender uses "<run_path>": to identify the main script
if __name__ == "<run_path>":
    # get the names of all objects in the collection 'Cameras'
    camera_names = [camera.name for camera in bpy.data.collections["Cameras"].objects]

    # This camera is used for the top down view to get a XZ plane view of the scene
    camera_names += ["Camera_TopDown"]

    # get the names of all top level objects in the collection 'Trucks'
    truck_names = [
        truck.name for truck in bpy.data.collections["Trucks"].objects if not truck.parent
    ]

    render_current_frame(camera_names, truck_names)
    # render_all_frames(camera_names, truck_names)

    print("Done with rendering, all files are saved in", OUTPUT_LOCATION.absolute())
