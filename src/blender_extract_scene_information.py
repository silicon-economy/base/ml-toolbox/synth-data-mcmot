# SPDX-FileCopyrightText: Fraunhofer IML
#
# SPDX-License-Identifier: GPL-3.0-or-later

# context.area: VIEW_3D

"""A script to extract information from a Blender scene.

The script uses the Blender API to extract information about the scene.
This includes the intrinsic and extrinsic camera parameters and transformation matrices
to convert between different coordinate systems.
Additionally, it extracts the camera positions and orientations in the scene.

All this information is saved to disk as numpy arrays for further processing.
"""
import math
from pathlib import Path

import bpy
import numpy as np
from mathutils import Euler, Matrix, Vector

# Path to the blender scene file in this repository. Change this as needed.
SCENE_PATH = Path(
    "./blender-scenes/complex-logistics-yard-truck-scene/complex-logistics-yard-truck-scene.blend"
)
# Path to the output directory of this script. Normally you don't need to change this.
OUTPUT_LOCATION = SCENE_PATH.parent / Path("output/")
# Path of the output directory for the camera matrices.
CAMERA_DATA_OUTPUT_LOCATION = OUTPUT_LOCATION / Path("camera_matrices/")
# Name of the top down camera looking down on the XY plane.
ORTOGRAPHIC_CAMERA_NAME = "Camera_TopDown"
# The z level of the XY plane. This might be 0 or some other value depending on the scene.
XY_PLANE_Z_LEVEL = -0.5


def handle_auto_sensor_fit(sensor_fit: str, render_res_x: float, render_res_y: float) -> str:
    """Handle the AUTO sensor fit option.

    Args:
        sensor_fit (str): the type of sensor fit
        render_res_x (float): render resolution in x direction
        render_res_y (float): render resolution in y direction

    Returns:
        str: the sensor fit type, either HORIZONTAL or VERTICAL
    """
    if sensor_fit == "AUTO":
        if render_res_x >= render_res_y:
            return "HORIZONTAL"
        else:
            return "VERTICAL"
    return sensor_fit


def intrinsic_calibration_parameters(camera: bpy.types.Camera) -> Matrix:
    """Calculates the intrinsic calibration parameters for a perspective camera.

    Args:
        camera (bpy.types.Camera): The Blender camera

    Returns:
        Matrix: Intrinsic calibration matrix.

    Raises:
        NotImplementedError: If the camera is not a perspective camera.
    """
    camera_data = camera.data
    if camera_data.type != "PERSP":
        raise NotImplementedError(
            f"Only perspective cameras are supported, not {camera_data.type}"
        )

    scene = bpy.context.scene
    render = scene.render

    # Calculate resolution scale and pixel dimensions
    scale = render.resolution_percentage / 100
    width_px = scale * render.resolution_x
    height_px = scale * render.resolution_y

    # Handle sensor fitting
    sensor_fit = handle_auto_sensor_fit(
        camera_data.sensor_fit,
        render.pixel_aspect_x * width_px,
        render.pixel_aspect_y * height_px,
    )

    aspect_ratio = render.pixel_aspect_y / render.pixel_aspect_x

    # Determine view factor in pixels
    view_factor_px = width_px if sensor_fit == "HORIZONTAL" else aspect_ratio * height_px

    # Calculate pixel size in millimeters
    lens_focal_length_mm = camera_data.lens
    sensor_dim_mm = (
        camera_data.sensor_height
        if camera_data.sensor_fit == "VERTICAL"
        else camera_data.sensor_width
    )
    pixel_size_mm = sensor_dim_mm / lens_focal_length_mm / view_factor_px

    # Compute scaling factors
    scaling_u = 1 / pixel_size_mm
    scaling_v = scaling_u / aspect_ratio

    # Calculate principal points
    principal_point_u = width_px / 2 - camera_data.shift_x * view_factor_px
    principal_point_v = height_px / 2 + camera_data.shift_y * view_factor_px / aspect_ratio

    # Assume zero skew for rectangular pixels
    pixel_skew = 0

    # Intrinsic calibration matrix
    intrinsic_matrix = Matrix(
        ((scaling_u, pixel_skew, principal_point_u), (0, scaling_v, principal_point_v), (0, 0, 1))
    )
    return intrinsic_matrix


def extrinsic_rotation_translation_parameters(cam: bpy.types.Camera) -> Matrix:
    """Calculate the camera rotation and translation matrix from a Blender camera.

    Args:
        cam (bpy.types.Camera): The Blender camera

    Returns:
        Matrix: A 3x4 transformation matrix
    """
    # Decompose the world matrix to get location and rotation quaternion
    cam_matrix_world: Matrix = cam.matrix_world
    location, rotation = cam_matrix_world.decompose()[0:2]

    # Convert quaternion to rotation matrix and transpose to get the inverse
    rotation_world_2_cam = rotation.to_matrix().transposed()

    # Convert camera location to translation vector for coordinate changes
    translation_world_2_cam = -1 * rotation_world_2_cam @ location

    # Blender camera to camera rotation matrix
    rotation_cam_2_cv = Matrix(((1, 0, 0), (0, -1, 0), (0, 0, -1)))

    # Compute rotation and translation from world to camera
    rotation_world_2_cv = rotation_cam_2_cv @ rotation_world_2_cam
    translation_world_2_cv = rotation_cam_2_cv @ translation_world_2_cam

    # Combine into a 3x4 transformation matrix
    transformation_matrix = Matrix(
        (
            rotation_world_2_cv[0][:] + (translation_world_2_cv[0],),
            rotation_world_2_cv[1][:] + (translation_world_2_cv[1],),
            rotation_world_2_cv[2][:] + (translation_world_2_cv[2],),
        )
    )

    return transformation_matrix


def world_to_orthographic_camera_projection_matrix(ortographic_camera: bpy.types.Camera) -> Matrix:
    """The 4x4 transformation matrix from world to camera coordinates for an ortographic camera.

    This can be used with homogeneous world coordinates (x, y, z, 1)
    to get normalized device coordinates (NDC) (x, y, z, 1)
    where (0, 0) is the bottom left and (1, 1) is the top right of the camera frame.
    Negative z values indicate that the object is behind the camera.

    This takes the x and y offset of the camera into account.

    Consider using bpy_extras.object_utils.world_to_camera_view for a simpler solution
    if you don't need the full transformation matrix
    or you need support for perspective cameras.

    Args:
        ortographic_camera (bpy.types.Camera): The ortographic camera

    Returns:
        Matrix: A 4x4 transformation matrix
    """
    if ortographic_camera.data.type != "ORTHO":
        raise ValueError("Camera is not ortographic")

    cam_matrix_world: Matrix = ortographic_camera.matrix_world

    camera = ortographic_camera.data
    scene = bpy.context.scene
    frame = [v for v in camera.view_frame(scene=scene)[:3]]

    min_x, max_x = frame[2].x, frame[1].x
    min_y, max_y = frame[1].y, frame[0].y

    # first translation by -min_x and -min_y using translation matrix
    translation_matrix = Matrix.Translation((-min_x, -min_y, 0))
    # then scale to 0-1
    scale_matrix_x = Matrix.Scale(1.0 / (max_x - min_x), 4, (1, 0, 0))
    scale_matrix_y = Matrix.Scale(1.0 / (max_y - min_y), 4, (0, 1, 0))
    scale_matrix_z = Matrix.Scale(-1, 4, (0, 0, 1))
    scale_matrix = scale_matrix_x @ scale_matrix_y @ scale_matrix_z

    return scale_matrix @ translation_matrix @ cam_matrix_world.normalized().inverted()


def normalized_device_coordinates_to_pixels_matrix() -> Matrix:
    """Convert normalized device coordinates (NDC) to pixels.

    NDC are coordinates where (0, 0) is the bottom left and (1, 1) is
    the top right of the camera frame.
    Negative z values indicate that the object is behind the camera.

    Returns:
        Matrix: A 4x4 transformation matrix
    """
    scene = bpy.context.scene
    scale = scene.render.resolution_percentage / 100
    scene_resolution_x_in_px = scale * scene.render.resolution_x
    scene_resolution_y_in_px = scale * scene.render.resolution_y

    # scale to scene resolution (in pixels)
    resolution_scale_matrix = Matrix.Scale(scene_resolution_x_in_px, 4, (1, 0, 0))
    resolution_scale_matrix @= Matrix.Scale(scene_resolution_y_in_px, 4, (0, 1, 0))

    # flip y axis (because the camera image is flipped in NDC)
    flip_y_matrix = Matrix.Translation((0, scene_resolution_y_in_px, 0)) @ Matrix.Scale(
        -1, 4, (0, 1, 0)
    )

    return flip_y_matrix @ resolution_scale_matrix


def cam_pix_to_orhto_cam_pix_matrix(
    cam_pix_2_xy_plane: Matrix,
    xy_plane_to_world: Matrix,
    world_to_orthographic_ndc: Matrix,
    ndc_to_pixels: Matrix,
) -> Matrix:
    """Compute the end to end transformation matrix from camera pixels to top down camera pixels.

    This is the final transformation matrix that can be used to transform from
    input camera pixels to the pixels of ortographic camera looking down on the XY plane.

    Args:
        cam_pix_2_xy_plane (Matrix): 4x4 transformation matrix from input camera pixels to XY plane
        xy_plane_to_world (Matrix): 4x4 transformation matrix from XY plane coordinates to world
        world_to_orthographic_ndc (Matrix): 4x4 transformation matrix from world coordinates to
                                            normalized device coordinates of the ortographic camera
        ndc_to_pixels (Matrix): 4x4 transformation matrix from NDC to pixels

    Returns:
        Matrix: A 4x4 transformation matrix
    """
    # project the object location (x, y, 1) to the XY plane (xw, yw, w)
    # where w is a homogeneous scaling factor
    projected_object_location_xy_plane = cam_pix_2_xy_plane
    # we could divide by w here to get the canonical coordinates (xw/w, yw/w, 1)
    # but this will make the operation dependent on the actual point we are projecting.
    # We don't want that, so we leave it as (xw, yw, w) and divide by w later.

    # project to world coordinates (xw, yw, zw, w)
    projected_object_location_world: Matrix = (
        xy_plane_to_world @ projected_object_location_xy_plane
    )

    # project to camera space coordinates (xw, yw, zw, w)
    # from 0 to 1
    camera_space_location_ndc: Matrix = world_to_orthographic_ndc @ projected_object_location_world

    # compute the final coordinates in camera pixels (xw, yw, zw, w)
    camera_image_location_pix: Matrix = ndc_to_pixels @ camera_space_location_ndc

    return camera_image_location_pix


def camera_independent_matrices() -> tuple[Matrix, Matrix, Matrix]:
    """Compute the camera independent matrices that rely only on the scene.

    Returns:
        Tuple[Matrix, Matrix, Matrix]: A tuple of the matrices
    """
    # Projection from the Z=XY_PLANE_Z_LEVEL plane coordinates (homogeneous, (x,y,1))
    # to world coordinates (homogeneous, (x,y,z,1) with z = XY_PLANE_Z_LEVEL)
    xy_plane_to_world = Matrix(((1, 0, 0), (0, 1, 0), (0, 0, XY_PLANE_Z_LEVEL), (0, 0, 1)))

    ndc_to_pixels = normalized_device_coordinates_to_pixels_matrix()

    # ortographic top down camera looking down on the XY plane
    ortographic_camera: bpy.types.Camera = bpy.data.objects[ORTOGRAPHIC_CAMERA_NAME]
    if ortographic_camera.data.type != "ORTHO":
        raise ValueError(f"Camera {ORTOGRAPHIC_CAMERA_NAME} is not ortographic")
    ortographic_camera_scale = ortographic_camera.data.ortho_scale
    if ortographic_camera_scale <= 0:
        raise ValueError(
            f"Camera {ORTOGRAPHIC_CAMERA_NAME} has invalid scale {ortographic_camera_scale=}"
        )
    world_to_orthographic_ndc = world_to_orthographic_camera_projection_matrix(ortographic_camera)

    return xy_plane_to_world, world_to_orthographic_ndc, ndc_to_pixels


def camera_point_to_orthographic_camera_point(
    camera_image_point: tuple[int, int],
    cam_pix_2_orhto_cam_pix_matrix: Matrix,
    scene_resolution_x_in_px: int,
    scene_resolution_y_in_px: int,
) -> tuple[int, int]:
    """Transform a point from camera image pixels to top down camera image pixels.

    Args:
        camera_image_point (Tuple[int, int]): The point in camera image pixels
        cam_pix_2_orhto_cam_pix_matrix (Matrix): The transformation matrix from
                                                 camera image pixels to
                                                 top down camera image pixels
        scene_resolution_x_in_px (int): The scene resolution in x direction
        scene_resolution_y_in_px (int): The scene resolution in y direction

    Raises:
        ValueError: If the camera image point is outside of the camera image

    Returns:
        Tuple[int, int]: The point in top down camera image pixels
    """
    camera_image_point_vec = Vector((camera_image_point[0], camera_image_point[1], 1))
    if camera_image_point_vec.x < 0:
        raise ValueError(f"{camera_image_point_vec.x=} coordinate must be >= 0")
    elif camera_image_point_vec.y < 0:
        raise ValueError(f"{camera_image_point_vec.y=} coordinate must be >= 0")
    elif camera_image_point_vec.x > scene_resolution_x_in_px:
        raise ValueError(
            f"{camera_image_point_vec.x=} coordinate must be <= {scene_resolution_x_in_px=}"
        )
    elif camera_image_point_vec.y > scene_resolution_y_in_px:
        raise ValueError(
            f"{camera_image_point_vec.y=} coordinate must be <= {scene_resolution_y_in_px=}"
        )

    # apply the transformation matrix to get the pixel coordinates (xw, yw, zw, w)
    camera_image_location: Vector = cam_pix_2_orhto_cam_pix_matrix @ camera_image_point_vec

    # finally divide by w to get the final pixel coordinates (x, y, z, 1)
    camera_image_location /= camera_image_location[3]

    return camera_image_location.to_2d().to_tuple()


def save_matrix(matrix: Matrix, filename: str) -> None:
    """Save a matrix to disk as a numpy array.

    Args:
        matrix (Matrix): The matrix to save
        filename (str): The filename without extension
    """
    matrix = np.array(matrix)
    matrix = matrix.astype(np.float32)
    save_path = (CAMERA_DATA_OUTPUT_LOCATION / Path(f"{filename}.npy")).absolute()
    print("Saving matrix to", save_path)
    np.save(save_path, matrix)


def extract_camera_positions_and_rotations(
    camera_object: bpy.types.Camera,
) -> tuple[np.ndarray, np.ndarray]:
    """Extracts the world coordinates and rotations of the cameras based on the given objects.

    Args:
        camera_object (bpy.types.Camera): A camera object in the scene.

    Returns:
        tuple: A tuple containing world coordinates and rotations (XYZ degree).
    """
    # Extract the world coordinates of the camera positions
    cam_pos = camera_object.matrix_world.translation
    # Add an additional dimension for homogeneity
    cam_pos_homogeneous = np.append(cam_pos, 1)

    # Extract the camera rotation
    cam_rotation: Euler = camera_object.rotation_euler
    xyz_rotations: list[float] = []
    for axis in "XYZ":
        axis_index = cam_rotation.order.index(axis)
        xyz_rotations.append(math.degrees(cam_rotation[axis_index]))

    print(f"Camera {camera_object.name} at position {cam_pos} with rotation {xyz_rotations}")

    return cam_pos_homogeneous, np.array(xyz_rotations)


def extract_camera_information() -> None:
    """Extract camera information from the Blender scene and save it to disk."""
    print("Starting Blender camera information extraction")

    # create output directory if it does not exist
    if not CAMERA_DATA_OUTPUT_LOCATION.exists():
        CAMERA_DATA_OUTPUT_LOCATION.mkdir(parents=True)
        print("Created output directory", CAMERA_DATA_OUTPUT_LOCATION.absolute())

    xy_plane_to_world, world_to_orthographic_ndc, ndc_to_pixels = camera_independent_matrices()
    save_matrix(xy_plane_to_world, "matrix_xy_plane_to_world")
    save_matrix(world_to_orthographic_ndc, "matrix_world_to_orthographic_ndc")
    save_matrix(ndc_to_pixels, "matrix_ndc_to_pixels")

    # Run for all cameras in the collection 'Cameras'
    camera_names = [camera.name for camera in bpy.data.collections["Cameras"].objects]
    print(f"Found {len(camera_names)} cameras in the scene")
    for camera_name in camera_names:
        print("Extracting transformation matrices for camera", camera_name)
        cam = bpy.data.objects[camera_name]

        world_pos, world_rot = extract_camera_positions_and_rotations(cam)
        save_matrix(world_pos, f"matrix_{camera_name}_world_position")
        save_matrix(world_rot, f"matrix_{camera_name}_world_rotation")

        intrinsic_parameters = intrinsic_calibration_parameters(cam)
        save_matrix(intrinsic_parameters, f"matrix_{camera_name}_intrinsic_camera_parameters")
        extrinsic_parameters = extrinsic_rotation_translation_parameters(cam)
        save_matrix(extrinsic_parameters, f"matrix_{camera_name}_extrinsic_camera_parameters")

        world_2_cam_pix = intrinsic_parameters @ extrinsic_parameters
        save_matrix(world_2_cam_pix, f"matrix_{camera_name}_world_to_pixel_coordinates")

        # Projection from XY plane coordinates (homogeneous, (x,y,1))
        # to camera coordinates (homogeneous, (x,y,1))
        xy_plane_2_cam_pix: Matrix = world_2_cam_pix @ xy_plane_to_world

        # find inverse matrix
        # this projects from camera coordinates (homogeneous, (x,y,1))
        # to XY plane coordinates (homogeneous, (x,y,1))
        try:
            cam_pix_2_xy_plane = Matrix(np.linalg.inv(xy_plane_2_cam_pix))
        except np.linalg.LinAlgError:
            print(
                "ERROR: Matrix is not invertible. "
                + "This means that the camera is not looking at the XY plane. "
                + "Maybe the camera is placed right in the plane?"
            )
            raise

        # this matrix is dependent on the camera
        cam_pix_2_orhto_cam_pix_matrix = cam_pix_to_orhto_cam_pix_matrix(
            cam_pix_2_xy_plane, xy_plane_to_world, world_to_orthographic_ndc, ndc_to_pixels
        )
        save_matrix(
            cam_pix_2_orhto_cam_pix_matrix, f"matrix_{camera_name}_cam_pix_to_orthographic_cam_pix"
        )

    print(
        "Done with extraction of all camera information from the scene, all files are saved in",
        CAMERA_DATA_OUTPUT_LOCATION.absolute(),
    )


if __name__ == "__main__":
    # The script is run from the command line, hence we need to open the scene
    bpy.ops.wm.open_mainfile(filepath=str(SCENE_PATH.absolute()))

    extract_camera_information()

# Blender uses "<run_path>": to identify the main script
if __name__ == "<run_path>":
    # The script is run using VsCode Blender debug configuration
    # hence we don't need to open the scene
    extract_camera_information()
