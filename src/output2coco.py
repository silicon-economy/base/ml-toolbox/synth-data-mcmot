# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

"""A script to generate a dataset in the official COCO annotation format from render output.

The script reads all MOT files in the `./output` directory and converts them to
a single COCO dataset.
Additionally, it reads all images in the `./output` directory and uses them as the dataset images.
The resulting dataset is exported to `./coco`.
"""
import csv
import os
import re
from pathlib import Path

import cv2
import fiftyone as fo
import numpy as np
from tqdm import tqdm

# take Camera_*_ImageRGB_0001.jpg and Camera_*_ImageRGB_0123.jpg
# but not Camera_*_ImageRGB_0001_occluded_bb.jpg

SCENE_PATH = Path("./blender-scenes/complex-logistics-yard-truck-scene/")
images_path = SCENE_PATH / Path("./output/")
coco_dataset_path = SCENE_PATH / Path("./coco/")

print(f"Total files in images_path '{images_path}':", len(os.listdir(images_path)))
plain_image_paths = [
    images_path.joinpath(Path(f))
    for f in os.listdir(images_path)
    if re.search(r"Camera_.+_ImageRGB_\d\d\d\d\.jpg$", f)
]

# get image size from first image using cv2
if len(plain_image_paths) > 0:
    print("Found", len(plain_image_paths), "plain_image_paths")
    image_size = np.array(cv2.imread(str(plain_image_paths[0]))).shape[:2][::-1]
else:
    raise Exception("No images found!")


annotations = {
    path: []
    for path in plain_image_paths
    # "directory/image000001.jpg": [
    #     {"bbox": ..., "label": ...},
    # ],
}


# read all MOT files with csv reader
for mot_file in tqdm(Path("./output/").glob("Camera_*_MOT.txt")):
    camera_name = re.match(r"(Camera_.+)_MOT.txt$", Path(mot_file).name).group(1)
    with open(mot_file) as f:
        csv_reader = csv.reader(f, delimiter=",")
        for row in csv_reader:
            # csv_writer.writerow([
            #     frame_number, # frame id
            #     track_ids[truck_name],  # track id
            #     bounding_box[0], # x
            #     bounding_box[1], # y
            #     bounding_box[2], # width
            #     bounding_box[3], # height
            #     1, # class id
            #     1.0, # score
            # ])
            frame_number = int(row[0])
            x = float(row[2])
            y = float(row[3])
            width = float(row[4])
            height = float(row[5])

            # normalize bounding box coordinates by image size
            x /= image_size[0]
            y /= image_size[1]
            width /= image_size[0]
            height /= image_size[1]

            annotations[Path(f"./output/{camera_name}_ImageRGB_{frame_number:04d}.jpg")].append(
                {
                    "bbox": [x, y, width, height],
                    "label": "truck",
                }
            )

# Create dataset
dataset: fo.Dataset = fo.Dataset(name=SCENE_PATH.name)
dataset.persistent = False

for filepath in tqdm(plain_image_paths):
    sample = fo.Sample(filepath=filepath)

    detections = []
    for annotation in annotations[filepath]:
        label = annotation["label"]
        bounding_box = annotation["bbox"]

        detections.append(fo.Detection(label=label, bounding_box=bounding_box))

    sample["ground_truth"] = fo.Detections(detections=detections)

    dataset.add_sample(sample)

export_dir = str(coco_dataset_path.absolute())
label_field = "ground_truth"

dataset.info

dataset.export(
    export_dir=export_dir,
    dataset_type=fo.types.COCODetectionDataset,
    label_field=label_field,
)
