# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3


"""A script to generate a dataset in the official MOT annotation format from render output.

Before running this script, you must render all desired images and MOT annotations
using blender_render_pipeline.py.
The output should be saved to the output/ directory.

The generated dataset is saved to mot_dataset/ in the current directory.
In there is a directory for each camera, containing the images
and a gt/ directory with a single gt.txt file containing the annotations.

mot_dataset
├── Camera_name_1
│   ├── gt
│   │   └── gt.txt
│   └── img1
│       ├── 000001.jpg
│       ├── 000002.jpg
│       ├── 000003.jpg
│       ...
├── Camera_name_2
│   ├── gt
│   │   └── gt.txt
│   └── img1
│       ├── 000001.jpg
│       ├── 000002.jpg
│       ├── 000003.jpg
│       ...
...

The script automatically adjusts the file names, including numbering the images starting at 1.
"""
import csv
import os
import re
import shutil
from pathlib import Path

from tqdm import tqdm

SCENE_PATH = Path("./blender-scenes/complex-logistics-yard-truck-scene/")
output_path = SCENE_PATH / Path("./output/")
mot_dataset_path = SCENE_PATH / Path("./mot_dataset/")

# find all Camera_*_MOT.txt files
mot_files = list(output_path.glob("Camera_*_MOT.txt"))
print("Found ", len(mot_files), " MOT files")

if len(mot_files) == 0:
    raise Exception(f"No MOT files found in {output_path}! Did you run main.py?")

camera_names = [
    str(re.match(r"(Camera_.+)_MOT.txt$", mot_file.name).group(1)) for mot_file in mot_files
]
assert len(set(camera_names)) == len(camera_names), "Duplicate camera names found!"
assert len(mot_files) == len(camera_names), "Mismatch between camera names and MOT files!"

plain_image_paths = [
    output_path.joinpath(Path(f))
    for f in os.listdir(output_path)
    if re.search(r"Camera_.+_ImageRGB_\d\d\d\d\.jpg$", f)
]

# find all images that belong to a camera
plain_images_per_camera = {
    camera_name: [image_path for image_path in plain_image_paths if camera_name in image_path.name]
    for camera_name in camera_names
}

# check that there is the same number of images per camera and at least one image per camera
if len(set([len(images) for images in plain_images_per_camera.values()])) == 1:
    raise Exception("Mismatch between number of images per camera.")
cameras_with_no_images = [
    camera_name for camera_name, images in plain_images_per_camera.items() if len(images) == 0
]
if len(cameras_with_no_images) > 0:
    raise Exception(f"No images found for cameras {cameras_with_no_images}.")

print("Found", len(plain_images_per_camera[camera_names[0]]), "images per camera.")


def generate_gt_file(mot_file: Path, gt_file: Path, frame_number_offset: int = 0) -> set:
    """Generate a gt.txt file from a MOT file.

    Args:
        mot_file (Path): The MOT file to read from.
        gt_file (Path): The gt.txt file to write to.
        frame_number_offset (int, optional): The frame number offset to use. Defaults to 0.

    Returns:
        set: The set of input frame numbers that were seen.
    """
    seen_frame_numbers = set()
    with open(mot_file) as f:
        csv_reader = csv.reader(f, delimiter=",")
        with open(gt_file, "w", newline="\n") as f:
            csv_writer = csv.writer(f, delimiter=",")
            for row in csv_reader:
                frame_number = int(row[0])
                track_id = int(row[1])
                x = int(row[2])
                y = int(row[3])
                width = int(row[4])
                height = int(row[5])
                class_id = int(row[6])
                score = float(row[7])

                seen_frame_numbers.add(frame_number)
                new_frame_number = frame_number + frame_number_offset

                csv_writer.writerow(
                    [
                        new_frame_number,
                        track_id,
                        x,
                        y,
                        width,
                        height,
                        class_id,
                        score,
                    ]
                )
    return seen_frame_numbers


for camera_name, mot_file in tqdm(
    list(zip(camera_names, mot_files)), desc="Generating MOT dataset"
):
    # create all directories
    camera_path = mot_dataset_path.joinpath(camera_name)
    gt_path = camera_path.joinpath("gt")
    gt_path.mkdir(parents=True, exist_ok=True)
    img1_path = camera_path.joinpath("img1")
    img1_path.mkdir(parents=True, exist_ok=True)

    # find the lowest frame number used by an image
    lowest_frame_number = min(
        [
            int(re.match(r".+ImageRGB_(\d\d\d\d)\.jpg$", image_path.name).group(1))
            for image_path in plain_images_per_camera[camera_name]
        ]
    )
    # if the lowest frame number is not 1, we need to adjust the frame numbers in the MOT file
    offset = 1 - lowest_frame_number

    if offset != 0:
        print(
            f"Adjusting frame numbers for camera {camera_name} by {offset} "
            f"to start at 1 instead of {lowest_frame_number}."
        )

    # copy the MOT file and adjust the frame numbers
    gt_file = gt_path.joinpath("gt.txt")
    seen_frame_numbers = generate_gt_file(mot_file, gt_file, frame_number_offset=offset)

    # copy all images and adjust the frame numbers
    for image_path in plain_images_per_camera[camera_name]:
        frame_number = int(re.match(r".+ImageRGB_(\d\d\d\d)\.jpg$", image_path.name).group(1))
        new_frame_number = frame_number + offset
        new_image_path = img1_path.joinpath(f"{new_frame_number:06d}.jpg")

        # copy the image
        shutil.copy(image_path, new_image_path)
