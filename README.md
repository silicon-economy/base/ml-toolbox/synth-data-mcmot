<!--
Copyright Open Logistics Foundation

SPDX-License-Identifier: OLFL-1.3-->

# Synthetic Data Generation for Multi-View Multi-Object Tracking
<figure>
  <img src="blender-scenes/complex-logistics-yard-truck-scene/assets/images/Camera_Building_Right_ImageRGB_0302_visible_bb.jpg" width="32%" alt="Building Right" />
  <img src="blender-scenes/complex-logistics-yard-truck-scene/assets/images/Camera_Gates_Right_ImageRGB_0302_visible_bb.jpg" width="32%" alt="Gate Right" />
  <img src="blender-scenes/complex-logistics-yard-truck-scene/assets/images/Camera_Opposite_Building_1_ImageRGB_0302_visible_bb.jpg" width="32%" alt="Opposite Building 1" />
  <img src="blender-scenes/complex-logistics-yard-truck-scene/assets/images/Camera_Building_Left_ImageRGB_0301_visible_bb.jpg" width="32%" alt="Building Left" />
  <img src="blender-scenes/complex-logistics-yard-truck-scene/assets/images/Camera_Gates_Left_ImageRGB_0302_visible_bb.jpg" width="32%" alt="Gate Left" />
  <img src="blender-scenes/complex-logistics-yard-truck-scene/assets/images/Camera_TopDown_ImageRGB_0302_visible_bb.jpg" width="32%" alt="Top View" />
  <figcaption>Visual representations of various camera views showcasing bounding box annotations for individual trucks.</figcaption>
</figure>


This repository contains the code to generate synthetic datasets for multi-view multi-object tracking 
using Blender and Python. 
The focus is on the multi-view aspect of the dataset, i.e. the same objects are visible from multiple cameras.


The dataset is compatible with the [MOT challenge](https://motchallenge.net/) format and can be used to validate
multi-view multi-object tracking systems.
Additionally, the dataset can be exported in common object detection formats such as COCO.


The primary focus of MovEyeData is on logistical yards (Yard-Logistics), making it particularly valuable for applications related to logistics and supply chain management. The synthetic datasets aim to simulate real-world scenarios encountered in yard logistics, enabling the development and testing of multi-view multi-object tracking models tailored to this specific domain and for creating user-defined scenarios.

For a detailed description see the documentation.

# Current state of the project
This project is actively under development. There will be new features, scenarios and improvements to be added in the future.

The following Scenes are currently available:

- simple-street: The scene consists of a street with moving vehicles and three cameras being partly occluded by a brick-wall. This scene is called `simple-street-scene`.
- complex-yard: This complex scene consists of a logistics yard,  multiple trucks moving from and to gates and multiple cameras. This scene is called `complex-logistics-yard-truck-scene`

 The following functionalities are currently available:

- Automated data Generation & Export: Save generated data in the
    - MOT (Multi Object Tracking) format for compatibility with multi-object tracking frameworks.
    - COCO (Common Objects in Context) format for compatibility with object detection frameworks.
    - Support for generating a video file from the data
- Automation: Python scripts for seamless data generation workflows.
- Camera Parameter: Automatically export the intrinsic and extrinsic camera parameters

The project is continuously evolving, with the following features planned for future releases:

- Integration with MOT Tracking Algorithms: Facilitate integration with popular MOT tracking algorithms for performance evaluation.
- Improved Vehicle Movements: The turning of wheels as well as the towing of swap bodies.
- Add multiple Vehicle types: In the future more trucks will be available for scene generation


# Getting Started
## Target environment
This repository is designed to be run on any computer being able to run blender. We suggest using an NVIDIA graphics card for faster image generation. Check the `Installation` chapter in the `./documentation` for important deployment details and the `Tutorial` chapter for the automatic generation of datasets from already included scenes.

## Components of MovEyeData

| Component | Description |
| --- | --- |
| VSCode | Use Visual Studio Code for convenient control and customization of scripts data generation parameters |
| Blender | Blender API integration for scene creation, rendering, and annotation |
<!-- | MOT Format Export | Automatically export generated data in the MOT format | -->

# Documentation
Please find the documentation of this project in `./documentation`. Follow the instructions to set up, customize, and utilize the synthetic data generation and annotation capabilities.

# Contact information
Maintainers:
- Tim Chilla <a href="mailto:tim.chilla@iml.fraunhofer?">tim.chilla@iml.fraunhofer.de</a>
- Tom Stein <a href="mailto:tom.stein@iml.fraunhofer.de?">tom.stein@iml.fraunhofer.de</a>
- Christian Pionzewski <a href="mailto:christian.pionzewski@iml.fraunhofer.de?">christian.pionzewski@iml.fraunhofer.de</a>

# License
Most of this Repository is licensed under:

Copyright Open Logistics Foundation
Licensed under the Open Logistics Foundation License 1.3.

Due to generated images, used assets in FILENAME.blend files and code importing blender, some files are licensed under other terms.

For a precise license description please have a look at the corresponding FILENAME.LICENSE file, or the header of the file. 
The licensing-scheme being used is https://reuse.software/.


## Licenses of third-party dependencies

The licenses used by this project's third-party dependencies are documented in the `third-party-licenses` directory.
This is done to encourage developers to check the licenses used by the third-party dependencies to ensure they do not conflict with the license of this project itself.
The directory contains the following files:

* `third-party-licenses.csv` - Contains the licenses used by this project's third-party dependencies.
  The content of this file is/can be generated.
* `third-party-licenses-summary.csv` - Contains a summary of the licenses used by this project's third-party dependencies.
* `third-party-licenses-complementary.csv` - Contains entries for third-party dependencies for which the licenses cannot be determined automatically or which have
  a different naming convention e.g. BSD-3-Clause instead of BSD License.
  The content of this file is maintained manually.

### Generating third-party license reports

This project uses [pip-licenses](https://pypi.org/project/pip-licenses/) to generate a file containing the licenses used by the third-party dependencies.
The `third-party-licenses/third-party-licenses.csv` file can be generated with the following command.
```shell
poetry run pip-licenses --ignore-packages 'Synthetic Data Generation'  --with-urls --format=csv --output-file="third-party-licenses/third-party-licenses.csv"
```

To generate a summary of the licenses used by the third-party dependencies, the following command can be used.
```shell
poetry run pip-licenses --ignore-packages 'Synthetic Data Generation'  --summary --format=csv --output-file="third-party-licenses/third-party-licenses-summary.csv"
```
