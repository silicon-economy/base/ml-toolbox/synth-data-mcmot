# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

[tool.poetry]
name = "Synthetic Data Generation"
version = "0.1.0"
description = "Synthetic Data Generation using Blender and Python for Multi-View Multi-Object Tracking"
authors = ["Tom Stein <tom.stein@iml.fraunhofer.de>", "Tim Chilla <tim.chilla@iml.fraunhofer.de>", "Christian Pionzewski <christian.pionzewski@iml.fraunhofer.de>"]
readme = "README.md"
packages = [{include = "src"}]
license = "Open Logistics License Version 1.3"

[tool.poetry.dependencies]
python = "^3.10,<3.11"
bpy = "^3.6.0"
mathutils = "^3.3.0"
numpy = "^1.25.1"
opencv-python = "^4.8.0.74"
fiftyone = "^0.23.8"
boto3 = "^1.33.11" # pin version of boto3 to avoid endless dependency resolution, see https://github.com/voxel51/fiftyone/issues/3145
tqdm = "^4.66.1"

[tool.poetry.group.dev.dependencies]
ruff = "^0.4.2"
black = "^24.4.2"
ipykernel = "^6.29.0"
pip-licenses = "^4.4.0"

[build-system]
requires = ["poetry-core"]
build-backend = "poetry.core.masonry.api"

[tool.ruff]
line-length = 99
lint.select = [
  "E",   # pycodestyle
  "F",   # pyflakes
  "UP",  # pyupgrade
  "D",   # pydocstyle
  "I",   # isort
  "N",   # pep8-naming
  "ANN", # flake8-annotations
  "W",   # warning
]

[tool.ruff.lint.pydocstyle]
convention = "google"

[tool.black]
line-length = 99
