<!--
Copyright Open Logistics Foundation

SPDX-License-Identifier: OLFL-1.3-->

# Complex Logistics Yard Truck Scene
This scene is a more sophisticated version of a logistics yard. It is built around a big warehouse with multiple loading docks.
There are several trucks driving around the yard and a lot of other objects such as trees, solar panels, etc.
The scene is animated and the trucks are driving around the yard. The animation is 1501 frames long and the trucks are driving around the yard for the whole duration of the animation.
The scene is rendered from eight different cameras and another orthographic camera from the top. The cameras are placed in a way that they are overlapping and the trucks are visible from multiple cameras.

![Image of the trucks with bounding boxes from the side](assets/images/Camera_Building_Right_ImageRGB_0302_visible_bb.jpg)
![Image of the trucks with bounding boxes from another building](assets/images/Camera_Opposite_Building_1_ImageRGB_0302_visible_bb.jpg)
![Top down camera image](assets/images/Camera_TopDown_ImageRGB_0302_visible_bb.jpg)


# Download
The ready rendered datasets can be downloaded here:
- Ouput directory (containing segmentation masks, bounding boxes, etc.): 
- Object Detection only (COCO): https://owncloud.fraunhofer.de/index.php/s/mnw4MgotJdu9fVs
- Tracking (MOT): https://owncloud.fraunhofer.de/index.php/s/0gj2LYSN0fOA6bQ
- Raw Segmentations: https://owncloud.fraunhofer.de/index.php/s/pPwVAaKHbXFktPc

# Attribution

### 90 Light Commercial Truck - Low poly model
"'90 Light Commercial Truck - Low poly model" (https://skfb.ly/ootyy) by Daniel Zhabotinsky is licensed under Creative Commons Attribution (http://creativecommons.org/licenses/by/4.0/).

### Solar energy for a logistics center
"Solar energy for a logistics center" (https://skfb.ly/owCLY) by Oleh Volyk is licensed under Creative Commons Attribution (http://creativecommons.org/licenses/by/4.0/).

### Sunflowers (Pure Sky)
"Sunflowers (Pure Sky)" (https://polyhaven.com/a/sunflowers_puresky) by Jarod Guest and Sergej Majboroda is licensed under Creative Commons Universal (https://creativecommons.org/publicdomain/zero/1.0/).

### Aerial Asphalt 01
"Aerial Asphalt 01" (https://polyhaven.com/a/aerial_asphalt_01) by Rob Tuytel is licensed under Creative Commons Universal (https://creativecommons.org/publicdomain/zero/1.0/).
