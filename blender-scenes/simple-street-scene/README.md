<!--
Copyright Open Logistics Foundation

SPDX-License-Identifier: OLFL-1.3-->

# Yard Synthetic Data Generation
This blender project is used to generate synthetic data for the movement of trucks in a dummy scene. 
The scene is a straight road with a truck driving from left to right and two trucks from right to left.
There are three overlapping cameras in the scene. One on the left side, one on the right side and one in the middle.
Additionally, there is a wall occluding the view of the left and middle cameras. 

![Images of the trucks with bounding boxes](assets/images/combined_rendered_images.jpg)
![Top down camera image](assets/images/Camera_TopDown_ImageRGB_0092_visible_bb.jpg)

# Download
The ready rendered datasets can be downloaded here:
- Object Detection only (COCO): https://owncloud.fraunhofer.de/index.php/s/T4fZNY3y9AetrAI
- Tracking (MOT): https://owncloud.fraunhofer.de/index.php/s/yHube5pN1FJefya

# Attribution

### '90 Light Commercial Truck - Low poly model
"'90 Light Commercial Truck - Low poly model" (https://skfb.ly/ootyy) by Daniel Zhabotinsky is licensed under Creative Commons Attribution (http://creativecommons.org/licenses/by/4.0/).

### Brick Wall Test
"Brick Wall Test" (https://skfb.ly/EzMp) by adamdart7 is licensed under Creative Commons Attribution (http://creativecommons.org/licenses/by/4.0/).

### Aerial Asphalt 01
"Aerial Asphalt 01" (https://polyhaven.com/a/aerial_asphalt_01) by Rob Tuytel is licensed under Creative Commons Universal (https://creativecommons.org/publicdomain/zero/1.0/).
